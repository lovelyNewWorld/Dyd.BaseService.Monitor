﻿using Dyd.BaseService.Monitor.Domain.PlatformManage.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Web.Models;
using XXF.BasicService.CertCenter;
using XXF.Db;

namespace Web.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 签名
        /// </summary>
        /// <param name="paralist">需要进行签名的参数</param>
        /// <param name="appsecret">app密钥</param>
        /// <returns></returns>
        public static string GetSign(Dictionary<string, string> paralist, string appsecret)
        {
            if (paralist == null)
            {
                return null;
            }

            List<string> para = new List<string>();
            foreach (var a in paralist)
            {
                para.Add(a.Key + a.Value);
            }
            para.Sort();
            string centerstr = appsecret + string.Concat(para) + appsecret;

            return LibCrypto.MD5(centerstr);
        }

        [HttpGet]
        public ActionResult Login(string appid, string sign, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //登录
        [HttpPost]
        public ActionResult Login(string appid, string sign, string returnUrl, string username, string password, string validate)
        {
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["Admin"].Contains(";" + username + "," + password + ";"))
                {
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(username, false, (int)FormsAuthentication.Timeout.TotalMinutes);
                    string enticket = FormsAuthentication.Encrypt(ticket);
                    HttpCookie auth = new HttpCookie(FormsAuthentication.FormsCookieName, enticket);
                    Response.AppendCookie(auth);
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        //return RedirectToAction("ConfigIndex", "Config");
                        return RedirectToAction("index", "TimeWatchLogApiDayReport");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "登陆出错,请咨询管理员。");
                    return View();
                }
            }
            catch (Exception exp)
            {
                ModelState.AddModelError("", "登陆出错,请咨询管理员。错误信息:" + exp.Message);
                return View();
            }
        }

        //登出
        public ActionResult Logout(string returnurl)
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Login");
        }

    }
}
